**Primero se debe crear una plataforma para poder pasar el nivel 1**

[![Captura-de-pantalla-2023-04-18-220458.png](https://i.postimg.cc/DyZ4KvM0/Captura-de-pantalla-2023-04-18-220458.png)](https://postimg.cc/fkGRcNZQ)

**Para el segundo nivel se debe crear diferentes plataformas para que estan puedan formar una escalera y se debe cambiar el color de la plataforma con un "background"**

[![Captura-de-pantalla-33.png](https://i.postimg.cc/50bq5cNz/Captura-de-pantalla-33.png)](https://postimg.cc/4K29ZSc4)

**Para el tercer nivel se debe crear varias escaleras y ademas que cada plataforma debe tener un color distinto**

[![Captura-de-pantalla-34.png](https://i.postimg.cc/bwfGSxvD/Captura-de-pantalla-34.png)](https://postimg.cc/WhSpQJgT)

**Para el cuarto nivel minimo se debe crear 8 plataformas para poder pasar al siguiente nivel y que cada plataforma debe tener un color distino**

[![Captura-de-pantalla-36.png](https://i.postimg.cc/mgVHxcbP/Captura-de-pantalla-36.png)](https://postimg.cc/zybBKGHN)

**Para el quinto nivel se debe alterar la longitud de las plataformas**

[![Captura-de-pantalla-38.png](https://i.postimg.cc/Z5PLWKJm/Captura-de-pantalla-38.png)](https://postimg.cc/216WJYq9)

**Nivel 6**

[![Captura-de-pantalla-39.png](https://i.postimg.cc/761CVbQP/Captura-de-pantalla-39.png)](https://postimg.cc/YG9q22HT)

**Nivel 7**

[![Captura-de-pantalla-41.png](https://i.postimg.cc/gjJYZ0CT/Captura-de-pantalla-41.png)](https://postimg.cc/BLr9WJQT)

